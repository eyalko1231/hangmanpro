
def func(num1, num2):
    """
    this function get 2 numbers and calc the sum of them
    :param num1 int:
    :param num2 int:
    :return the sum:
    :rtype int
    """
    return num1 + num2


def main():
    print(func(1,2))


if __name__ == '__main__':
    main()
