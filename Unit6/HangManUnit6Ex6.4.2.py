def try_update_letter_guessed(letter_guessed, old_letters_guessed):
    '''
        this function gets a letter and the list of letter and checks if the given letter
        is in the list.
        if not append it into the list, otherwise print X and the letter in the list
        :param letter_guessed:
        :param old_letters_guessed:
        :return boolean if it added to the list:
        '''
    if(check_valid_input(letter_guessed,old_letters_guessed)):
        list(old_letters_guessed).append(letter_guessed)
        return True
    print("X\n")
    print("->".join(list(sorted(old_letters_guessed))))
    return False
