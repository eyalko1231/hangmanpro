

def inverse_dict(my_dict):
    '''
    this function replace the keys in the values and vice versa
    the new keys are lists
    :param dict my_dict:
    :return the inverse dicts:
    ;:rtype dict
    '''
    dic = {}
    for k, v in dict(my_dict).items():
        keys_list = []
        for k1, v1 in dict(my_dict).items():
            if(v1 == v):
                keys_list.append(k1)
        dic[v] = keys_list
    return dic



course_dict = {'I': 3, 'love': 3, 'self.py!': 2}
print(inverse_dict(course_dict))