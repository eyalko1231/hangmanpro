
def choose_word(file_path, index):
    '''
    this function gets filepath and index ,the return the word in that index
    and the amount of diffrent words in file
    :param file_path:
    :param index:
    :return:
    '''
    file = open(file_path,"r")
    words_list = []
    my_word = ""
    counter = 1
    text = file.read().split(" ")
    isfound = True
    for word in text:
        counter = counter % len(text)
        if(counter == index and isfound):
            my_word = word
            isfound = False
        if word not in words_list:
            words_list.append(word)
        counter += 1
    return (len(words_list),my_word)


print(choose_word(r"work2.txt",3))