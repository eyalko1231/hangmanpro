
def are_files_equal(file1, file2):
    '''
    this function gets paths to 2 files and checks if they are same
    :param string file1:
    :param string file2:
    :return if the files are same:
    :rtype bool
    '''
    file1open = open(file1, "r")
    file2open = open(file2, "r")
    if(file1open.read() != file2open.read()):
        file1open.close()
        file2open.close()
        return False
    file1open.close()
    file2open.close()
    return True

print(are_files_equal(r"C:\Users\Eyal\PycharmProjects\Hangman\venv\Include\work1.txt" , r"C:\Users\Eyal\PycharmProjects\Hangman\venv\Include\work2.txt"))