


def dodo(path_to_file, func):
    '''
    this function gets the path for the file and print as by the func
    :param string path_to_file:
    :param string func:
    :return:
    '''
    if(func == "sort"):
        sort_file(path_to_file)
    elif(func == "rev"):
        rev_lines(path_to_file)
    else:
        n = input("Enter last lines number: ")
        last_lines(path_to_file, n)





def last_lines(path_to_file, n):
    '''
    print only the n last lines
    :param path_to_file:
    :param n:
    :return:
    '''
    file = open(path_to_file,"r")
    for line in (file.readlines()[int(n):]):
        print(line)

def rev_lines(path_to_file):
    '''
    prints the lines reversed
    :param path_to_file:
    :return:
    '''
    file = open(path_to_file, "r")
    for line in file:
        print(line[::-1])
    file.close()


def sort_file(path_to_file):
    '''
    prints the words in a list without duplicates
    :param path_to_file:
    :return:
    '''
    result_list = []
    file = open(path_to_file, "r")
    temp_list = file.read().split(" ")
    file.close()
    [result_list.append(item) for item in temp_list if item not in result_list]
    print(result_list)


path_to_file = input("Enter path: ")
func = input("What do you want to do: ")
dodo(path_to_file, func)