
def copy_file_content(source, destination):
    '''
    this function copies the lines in source file into destination
    :param string source:
    :param string destination:
    :return:
    '''
    copy = open(source,"r")
    paste = open(destination,"w")
    paste.write(copy.read())
    copy.close()
    paste.close()



copy_file_content("work1.txt","work2.txt")