def sequence_del(my_str):
    '''
    this function get an str and returns it without duplicates
    :param my_str:
    :return: str
    '''
    not_dupli = list()
    for letter in my_str:
        if letter not in not_dupli:
            not_dupli.append(letter)
    return "".join(not_dupli)
